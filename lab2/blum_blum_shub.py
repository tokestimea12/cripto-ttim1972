def blum_blum_shub(s, length):
    p = 6131
    q = 11699
    n = p * q

    x = (s ** 2) % n

    byte_list = []
    for i in range(length):
        ch = '0'
        for j in range(7):
            x = x ** 2 % n
            ch += str(x % 2)
        byte_list.append(int(ch, 2))

    s = x
    
    return bytes(byte_list)

def blum_blum_shub_offset(s, length):
    p = 6131
    q = 11699
    n = p * q

    x = (s ** 2) % n

    for i in range(length):
        for j in range(7):
            x = x ** 2 % n
    
    s = x

