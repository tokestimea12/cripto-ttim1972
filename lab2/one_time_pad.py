from typing import Text
import solitaire
import blum_blum_shub

def one_time_pad(algorithm, old, key):
    new = []
    n = len(old)
    binary_old = bytes(old, 'ascii')

    if algorithm == 'S':
        byte_list = solitaire.get_bytes(key, n)
    elif algorithm == 'BBS':
        byte_list = blum_blum_shub.blum_blum_shub(key, n)

    for i in range(n):
        new.append(chr(binary_old[i] ^ byte_list[i]))

    return ''.join(new)

def one_time_pad_offset(algorithm, key, offset):

    if algorithm == 'S':
        for i in range(offset):
            solitaire.get_bytes_offset(key, offset)
    elif algorithm == 'BBS':
        for i in range(offset):
            blum_blum_shub.blum_blum_shub_offset(key, offset)