import socket
import configparser
import json
from one_time_pad import one_time_pad, one_time_pad_offset

#read config file
parser = configparser.ConfigParser()
parser.read('config.txt')
alg = parser.get('config','algorithm')
key = parser.get('config','key')
key = json.loads(key)		

#connect to the other client
s = socket.socket()				
s.connect(('localhost', 6666))
print('Connected to the communication on port 6666...')

#initial offset is 0
offset = 0

print('You will receive a message from the client you are connected with! Please wait.')
print('You can exit the conversation by typing EXIT when it is your turn to send a message.')

while True:
    #getting a message
    response = s.recv(1024).decode()
    response = json.loads(response)

    print('Received message is:', response)

    msg = response['msg']
    print('The encrypted message you got is: ' + msg)
    if msg == 'EXIT':
        print('The other client left the chat. :(')
        s.close()
        break

    response_offset = response['offset']
    if offset > response_offset:
        print('Your offset is bigger than the response offset. Fixing it now ...')
        offset = response_offset
        
        #read initial key from config file again
        parser = configparser.ConfigParser()
        parser.read('config.txt')
        key = parser.get('config','key')
        key = json.loads(key)
        one_time_pad_offset(alg, key, offset)
    
    elif offset < response_offset:
        print('Your offset is smaller than the response offset. Fixing it now ...')
        one_time_pad_offset(alg, key, response_offset - offset)
        offset = response_offset

    msg = one_time_pad(alg, msg, key)
    print('Your decrypted message is: ' + msg)

    offset += len(msg)	

    #sending a response
    print('Send a message ...')

    msg = input()

    if msg == 'EXIT':
        print('Bye! :)')
        s.sendall(json.dumps({
            'offset': offset,
            'msg': 'EXIT'
        }).encode())
        s.close()
        break
    
    print('You typed the following message: ' + msg)
    msg = one_time_pad(alg, msg, key)
    print('Your encrypted message is: ' + msg)

    s.sendall(json.dumps({
        'offset': offset,
        'msg': msg
    }).encode())
    print('Message sent! Wait for response.')

    offset += len(msg)
	
