import socket
import configparser
import json
from one_time_pad import one_time_pad, one_time_pad_offset

#read config file
parser = configparser.ConfigParser()
parser.read('config.txt')
alg = parser.get('config','algorithm')
key = parser.get('config','key')
key = json.loads(key)

#create connection socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind(('localhost', 6666))
s.listen(1)	
print('Starting communication on port 6666 ...')

client, addr = s.accept()			
print('The other client with address', addr, 'is now connected to you!')

#initial offset is 0
offset = 0

print('Please send a message to the other client!')
print('You can also exit the conversation by typing EXIT.')

while True:
    #sending a message
    print('Send a message ...')

    msg = input()

    if msg == 'EXIT':
        print('Bye! :)')
        client.sendall(json.dumps({
            'offset': offset,
            'msg': 'EXIT'
        }).encode())
        client.close()
        break
    
    print('You typed the following message: ' + msg)
    msg = one_time_pad(alg, msg, key)
    print('Your encrypted message is: ' + msg)

    client.sendall(json.dumps({
        'offset': offset,
        'msg': msg
    }).encode())
    print('Message sent! Wait for response.')

    offset += len(msg)

    #getting a response
    response = client.recv(1024).decode()
    response = json.loads(response)

    print('Received message is:', response)

    msg = response['msg']
    print('The encrypted message you got is: ' + msg)
    if msg == 'EXIT':
        print('The other client left the chat. :(')
        s.close()
        break

    response_offset = response['offset']
    if offset > response_offset:
        print('Your offset is bigger than the response offset. Fixing it now ...')
        offset = response_offset
        
        #read initial key from config file again
        parser = configparser.ConfigParser()
        parser.read('config.txt')
        key = parser.get('config','key')
        key = json.loads(key)
        one_time_pad_offset(alg, key, offset)
    
    elif offset < response_offset:
        print('Your offset is smaller than the response offset. Fixing it now ...')
        one_time_pad_offset(alg, key, response_offset - offset)
        offset = response_offset

    msg = one_time_pad(alg, msg, key)
    print('Your decrypted message is: ' + msg)

    offset += len(msg)
