import json
import socket
import threading

clients_data = {}

def server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('localhost', 8000))
    print('Server started on port 8000 ...')
    print('Waiting for connections ...')
    server.listen(1)

    while True:
        client, address = server.accept()
        threading.Thread(target=new_client, args=(client, )).start()

def new_client(client):
    print(f'{threading.get_ident()} - Opened')

    client.sendall(str('Welcome to the server!').encode())
    while True:
        msg = client.recv(1024).decode()
        msg = json.loads(msg)

        request = msg['request']
        
        if request == 'registration':
            id = msg['id']
            public_key = msg['pubKey']
            clients_data[id] = public_key
            client.sendall(str('Registered succesfully!').encode())
            print(f'{threading.get_ident()} - Client registered succesfully')
        elif request == 'getKey':
            client_id = msg['clientId']
            while True:
                if client_id in clients_data:
                    break
            client.sendall(json.dumps({
                'pubKey': clients_data[client_id],
            }).encode())
            print(f'{threading.get_ident()} - A public key was sent to the client')
            break
    client.close()

if __name__ == '__main__':
    server()