import socket
import configparser
import json
from solitaire import solitaire, solitaire_offset
from knapsack import *

PORT = 8001

def generate_random_secret():
    deck = list(range(1,28))
    random.shuffle(deck)
    return deck

def client_communication(server):
    #create connection socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    s.bind(('localhost', PORT))
    s.listen(1)	
    print('Starting communication on port ' + str(PORT) + ' ...')

    client, addr = s.accept()			
    print('The other client with address', addr, 'is now connected to you!')

    print('Sending request to get the public key of client 2 ...')
    server.sendall(json.dumps({
        'request': 'getKey',
        'clientId': PORT + 1,
    }).encode())

    data = server.recv(1024).decode()
    data = json.loads(data)
    print('Got the public key of client 2!')

    server.close()

    #get public key of client 2
    public_key2 = data['pubKey']

    #generate a random secret
    key1 = generate_random_secret()
    key1_encrypted = encrypt_knapsack(str(key1), public_key2)

    #send the half secret to client 2
    client.sendall(json.dumps({
        'secret': key1_encrypted,
    }).encode())

    #receive the other half from client 2
    data = client.recv(1024).decode()
    data = json.loads(data)
    key2 = decrypt_knapsack(data['secret'], private_key)
    key2 = json.loads(key2)

    #the common key will be the concatenation of the two half secrets
    print(key1)
    print(key2)
    key = key1 + key2

    print(key)

    #initial offset is 0
    offset = 0

    print('Please send a message to the other client!')
    print('You can also exit the conversation by typing EXIT.')

    while True:
        #sending a message
        print('Send a message ...')

        msg = input()

        if msg == 'EXIT':
            print('Bye! :)')
            client.sendall(json.dumps({
                'offset': offset,
                'msg': 'EXIT'
            }).encode())
            client.close()
            break
        
        print('You typed the following message: ' + msg)
        msg = solitaire(msg, key)
        print('Your encrypted message is: ' + msg)

        client.sendall(json.dumps({
            'offset': offset,
            'msg': msg
        }).encode())
        print('Message sent! Wait for response.')

        offset += len(msg)

        #getting a response
        response = client.recv(1024).decode()
        response = json.loads(response)

        print('Received message is:', response)

        msg = response['msg']
        print('The encrypted message you got is: ' + msg)
        if msg == 'EXIT':
            print('The other client left the chat. :(')
            s.close()
            break

        response_offset = response['offset']
        if offset > response_offset:
            print('Your offset is bigger than the response offset. Fixing it now ...')
            offset = response_offset
            
            #read initial key from config file again
            parser = configparser.ConfigParser()
            parser.read('config.txt')
            key = parser.get('config','key')
            key = json.loads(key)
            solitaire_offset(key, offset)
        
        elif offset < response_offset:
            print('Your offset is smaller than the response offset. Fixing it now ...')
            solitaire_offset(key, response_offset - offset)
            offset = response_offset

        msg = solitaire(msg, key)
        print('Your decrypted message is: ' + msg)

        offset += len(msg)

def connect_to_keyserver(server):			
    server.connect(('localhost', 8000))
    print('Connected to the server on port 8000...')
    msg = server.recv(1024).decode()
    print('Got this message: ' + msg)

def register_pub_key(public_key, server):
    print('Registering ...')
    server.sendall(json.dumps({
        'request': 'registration',
        'id': PORT,
        'pubKey': public_key
    }).encode())
    msg = server.recv(1024).decode()
    print(msg)

if __name__ == '__main__':
    private_key, public_key = generate_knapsack_keypair()
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	
    connect_to_keyserver(server)
    register_pub_key(public_key, server)
    client_communication(server)


