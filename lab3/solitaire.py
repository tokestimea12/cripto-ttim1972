def move_white_joker(deck):
    i = deck.index(53)

    if i < 53:
        deck[i], deck[i+1] = deck[i+1], deck[i]
    else:
        deck.remove(53)
        deck.insert(1, 53)

def move_black_joker(deck):
    i = deck.index(54)

    if i < 52:
        deck[i], deck[i+1] = deck[i+1], deck[i]
        deck[i+1], deck[i+2] = deck[i+2], deck[i+1]
    elif i == 52:
        deck.remove(54)
        deck.insert(1, 54)
    else:
        deck.remove(54)
        deck.insert(2, 54)

def swap_according_to_jokers(deck):
    joker1 = deck.index(53)
    joker2 = deck.index(54)

    if joker1 > joker2:
        joker1, joker2 = joker2, joker1
    
    joker2 += 1
    deck[joker2:], deck[:joker1] = deck[:joker1], deck[joker2:]

def swap_according_to_last(deck):
    last = deck[53]

    deck[last:53], deck[:last] = deck[:last], deck[last:53]

def solitaire_steps(deck):
    move_white_joker(deck)
    move_black_joker(deck)
    swap_according_to_jokers(deck)
    swap_according_to_last(deck)

def get_number(deck):
    while True:
        solitaire_steps(deck)
        first = deck[0]

        if first < 53:
            return deck[first] % 2

def get_bytes(deck, n):
    byte_list = []

    for _ in range(n):
        ch = '0'
        for j in range(7):
            nr = get_number(deck)
            ch += str(nr)
        byte_list.append(int(ch, 2))
    
    return bytes(byte_list)

def get_bytes_offset(deck, n):
    for _ in range(n):
        for _ in range(7):
            nr = get_number(deck)

def solitaire(old, key):
    new = []
    n = len(old)
    binary_old = bytes(old, 'ascii')

    byte_list = get_bytes(key, n)

    for i in range(n):
        new.append(chr(binary_old[i] ^ byte_list[i]))

    return ''.join(new)

def solitaire_offset(key, offset):
    for _ in range(offset):
        get_bytes_offset(key, offset)
    