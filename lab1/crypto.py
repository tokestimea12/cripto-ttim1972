#!/usr/bin/env python -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: TOKES TIMEA
SUNet: ttim1972

Replace this with a description of the program.
"""
import utils

# Caesar Cipher

def encrypt_caesar(plaintext):
    """Encrypt plaintext using a Caesar cipher.

    I iterated through all the characters of the plain text and rotated them to the right with 3.
    For this I used the abc list which contains all the alphabetic characters in upper-case.
    I left the non-alphabetic characters as they initially were.
    """
    if (len(plaintext) == 0):
        print('Then length of the plain text should be greater than 0!')
        return
    if (plaintext.isupper() == False):
        print('All alphabetic characters should be in upper case!')
        return
    ciphertext = ''
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for character in plaintext:
        if (character >= 'A' and character <= 'Z'):
            character = abc[(abc.index(character) + 3) % 26]
        ciphertext += character
    return ciphertext

def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    I iterated through all the characters of the cipher text and rotated them to the left with 3.
    For this I used the abc list which contains all the alphabetic characters in upper-case.
    I left the non-alphabetic characters as they initially were.
    """
    if (len(ciphertext) == 0):
        print('Then length of the cipher text should be greater than 0!')
        return
    if (ciphertext.isupper() == False):
        print('All alphabetic characters should be in upper case!')
        return
    plaintext = ''
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for character in ciphertext:
        if (character >= 'A' and character <= 'Z'):
            character = abc[(abc.index(character) - 3) % 26]
        plaintext += character
    return plaintext

def encrypt_caesar_binary(filename):
    """Encrypt binary file using a Caesar cipher.

    Encrypting binary files in Caesar cipher is like encrypting a plain text, only there will be more characters, in this case 256 instead of 26.
    I rotate every byte to the right with 3.
    """
    file = open(filename, "rb")

    byte = file.read(1)
    cipher = []
    while byte:
        byte = (int.from_bytes(byte,'little') + 3) % 256
        cipher.append(byte)
        byte = file.read(1)
    return bytes(cipher)

def decrypt_caesar_binary(filename):
    """Decrypt a binary file using a Caesar cipher.

    Decrypting binary files in Caesar cipher is like decrypting a plain text, only there will be more characters, in this case 256 instead of 26.
    I rotate every byte to the left with 3.
    """
    file = open(filename, "rb")

    byte = file.read(1)
    original = []
    while byte:
        byte = (int.from_bytes(byte,'little') - 3) % 256
        original.append(byte)
        byte = file.read(1)
    return bytes(original)

# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    I went through all the charachers of the plain text and rotated them to the right with the specific amount.
    For this I used the i variable which I initialized with 0.
    This specified which index I am at in the keyword.
    If the keyword was shorter than my text, I set i to 0 again so I started again at the beginning of my keyword.
    The rotation went on like this: for every character of the plain text, I added the index-th character of the keyword.
    """
    if (plaintext.isalpha() == False):
        print('Plain text should contain only alphabetic characters!')
        return
    if (keyword.isalpha() == False):
        print('Keyword should contain only alphabetic characters!')
        return
    if (plaintext.isupper() == False):
        print('All characters should be in upper case in the plain text!')
        return
    if (keyword.isupper() == False):
        print('All characters should be in upper case in the keyword!')
        return
    if (len(plaintext) == 0):
        print('Then length of the plain text should be greater than 0!')
        return
    if (len(keyword) == 0):
        print('Then length of the keyword should be greater than 0!')
        return
    ciphertext = ''
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    i = 0
    for character in plaintext:
        character = abc[(abc.index(character) + abc.index(keyword[i])) % 26]
        i += 1
        if (i == len(keyword)):
            i = 0
        ciphertext += character
    return ciphertext
    
def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    I went through all the charachers of the cipher text and rotated all of them to the left with a specific amount.
    For this I used the i variable which I initialized with 0.
    This  i specified which index I am at in the keyword.
    If the keyword was shorter than my text, I set i to 0 again so I started again at the beginning of my keyword.
    The rotation went on like this: for every character of the plain text, I subtracted the index-th character of the keyword.
    """
    if (ciphertext.isalpha() == False):
        print('Cipher text should contain only alphabetic characters!')
        return
    if (keyword.isalpha() == False):
        print('Keyword should contain only alphabetic characters!')
        return
    if (ciphertext.isupper() == False):
        print('All characters should be in upper case in the cipher text!')
        return
    if (keyword.isupper() == False):
        print('All characters should be in upper case in the keyword!')
        return
    if (len(ciphertext) == 0):
        print('Then length of the cipher text should be greater than 0!')
        return
    if (len(keyword) == 0):
        print('Then length of the keyword should be greater than 0!')
        return
    plaintext = ''
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    i = 0
    for character in ciphertext:
        character = abc[(abc.index(character) - abc.index(keyword[i])) % 26]
        i += 1
        if (i == len(keyword)):
            i = 0
        plaintext += character
    return plaintext

# Scytale Cipher

def encrypt_scytale(plaintext, circumference):
    """Encrypt plaintext using a Scytale cipher.

    Firstly, if the plain text is not a perfect multiple of the circumerence, I complete the text with some X-es in order to have a perfect multiple.
    Then I build the cypher text in more sequences.
    These sequences represent different parrts of the cypher text.
    I am iterating through the plain text from the i-th index (i goes from 0 to circumference -> first characters of the plain text).
    I finish the sequence when I reached the length of the plain text. The step I take every iteration is the size of the circumference.
    For example:
        I start with 0, then take circ. size steps while not reaching the plain text lenght.
        When reaching it, I start over with index 1 and so on ...
    """
    while len(plaintext) % circumference != 0:
        plaintext += 'X'
    ciphertext = []
    for i in range(circumference):
        seq = [plaintext[j] for j in range(i, len(plaintext), circumference)]
        ciphertext += seq
    return ''.join(ciphertext)

def decrypt_scytale(ciphertext, circumference):
    """Decrypt ciphertext using a Scytale cipher.

    The decryption of Scytale cypher is almost the same as the encryption.
    The only difference is that now I switch the 'rows' with the 'columns'.
    In order to do this I change the circumference to the division of the text with the original circumference.
    As result, I will take different sized steps between the charachers.
    We don't need to complete the plain text with X-es because it was already done in the encryption (if it was necessary).
    """
    plaintext = []
    circumference = len(ciphertext) // circumference
    for i in range(circumference):
        seq = [ciphertext[j] for j in range(i, len(ciphertext), circumference)]
        plaintext += seq
    return ''.join(plaintext)

# Railfence Cipher

def encrypt_railfence(plaintext, num_rails):
    """Encrypt plaintext using a Railfence cipher.

    For this encription I built the ciphertext by the rows of the zig-zag the plain text created.
    I used two variables: first and second which specified the size of the step i take between the characters of the plain text.
    For the firt and last row I had to use only one of this variables but in the rows between, the step sizes varied.
    After every finished row I had to change this variables by adding or substracting 2, because this was the step change between them.
    """
    ciphertext = ''
    first = num_rails * 2 - 2
    second = 0
    for i in range(0,len(plaintext),first):
        ciphertext += plaintext[i]
    first -= 2
    second += 2
    for i in range(1,num_rails-1,1):
        for j in range(i,len(plaintext),first+second):
            ciphertext += plaintext[j]
            ciphertext += plaintext[j+second]
        first -= 2
        second += 2

    for i in range(num_rails-1,len(plaintext),second):
        ciphertext += plaintext[i]

    return ciphertext

def decrypt_railfence(ciphertext, num_rails):
    """Decrypt ciphertext using a Railfence cipher.

    The dectryption is very similar to the encryption, but for this I had to use a matrix, too.
    Fisrtly, I initialised the matrix and iterated through the ciphertext and placed the character on the next index(represented by the variable c) on the specified place.
    I determined the places where I need to put the characters just like in the encryption algorithm.
    Next, I iterated through the columns of the matrix and got the plain text like this.
    """
    plaintext = ''
    matrix = [[' ' for x in range(len(ciphertext))] for y in range(num_rails)]
    first = num_rails * 2 - 2
    second = 0
    c = 0
    for i in range(0,len(ciphertext),first):
        matrix[0][i] = ciphertext[c]
        c += 1
    first -= 2
    second += 2
    for i in range(1,num_rails-1,1):
        for j in range(i,len(ciphertext),first+second):
            matrix[i][j] = ciphertext[c]
            matrix[i][j+second] = ciphertext[c + 1]
            c += 2
        first -= 2
        second += 2

    for i in range(num_rails-1,len(ciphertext),second):
        matrix[num_rails-1][i] = ciphertext[c]
        c += 1
    
    for i in range(len(ciphertext)):
        for j in range(num_rails):
            if (matrix[j][i] != ' '):
                plaintext += matrix[j][i]
    return plaintext


# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here

